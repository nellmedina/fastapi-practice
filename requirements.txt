-i https://pypi.org/simple
anyio==3.6.2 ; python_full_version >= '3.6.2'
async-generator==1.10 ; python_version >= '3.5'
attrs==22.2.0 ; python_version >= '3.6'
certifi==2022.12.7 ; python_version >= '3.6'
cffi==1.15.1 ; os_name == 'nt' and implementation_name != 'pypy'
click==8.1.3 ; python_version >= '3.7'
colorama==0.4.6 ; platform_system == 'Windows'
exceptiongroup==1.1.0 ; python_version < '3.11'
fastapi==0.92.0
greenlet==2.0.2 ; platform_machine == 'aarch64' or (platform_machine == 'ppc64le' or (platform_machine == 'x86_64' or (platform_machine == 'amd64' or (platform_machine == 'AMD64' or (platform_machine == 'win32' or platform_machine == 'WIN32')))))
h11==0.14.0 ; python_version >= '3.7'
idna==3.4 ; python_version >= '3.5'
outcome==1.2.0 ; python_version >= '3.7'
psycopg2-binary==2.9.5
pycparser==2.21
pydantic==1.10.5
pysocks==1.7.1
robotframework==6.0.2
robotframework-pythonlibcore==4.1.2 ; python_version >= '3.7' and python_version < '4'
robotframework-seleniumlibrary==6.0.0
selenium==4.8.2
sniffio==1.3.0 ; python_version >= '3.7'
sortedcontainers==2.4.0
sqlalchemy==2.0.5.post1
starlette==0.25.0 ; python_version >= '3.7'
trio==0.22.0 ; python_version >= '3.7'
trio-websocket==0.9.2 ; python_version >= '3.5'
typing-extensions==4.5.0 ; python_version >= '3.7'
urllib3[socks]==1.26.14 ; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3, 3.4, 3.5'
uvicorn==0.20.0
wsproto==1.2.0 ; python_full_version >= '3.7.0'
