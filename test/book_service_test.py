import json
from src.service import book_service
import pytest


def test_create_book(mock_server, monkeypatch):
    book_data = {"title": "something", "description": "something else", "id": 1}

    def mock_create_book(db, book):
        return book_data

    monkeypatch.setattr(book_service, "create_book", mock_create_book)

    response = mock_server.post("/book/add", data=json.dumps(book_data))
    assert response.status_code == 201
    assert response.json() == book_data


def test_create_book_input_validation(mock_server):
    response = mock_server.post("/book/add", data=json.dumps({"title": "something"}))
    assert response.status_code == 422

    response = mock_server.post("/book/add", data=json.dumps({"title": "1", "description": "aa"}))
    assert response.status_code == 422


def test_get_book_all(mock_server, monkeypatch):
    book_data_1 = {"title": "something", "description": "something else", "id": 1}
    book_data_2 = {"title": "something", "description": "something else", "id": 2}
    books = [book_data_1, book_data_2]

    def mock_get_book(db, skip, limit):
        return books

    monkeypatch.setattr(book_service, "get_book", mock_get_book)

    response = mock_server.get("/book/all")
    assert response.status_code == 200
    assert response.json() == books


def test_get_book(mock_server, monkeypatch):
    book_data = {"title": "something", "description": "something else", "id": 1}

    def mock_get_book_by_id(db, id):
        return book_data

    monkeypatch.setattr(book_service, "get_book_by_id", mock_get_book_by_id)

    response = mock_server.get("/book/1")
    assert response.status_code == 200
    assert response.json() == book_data


def test_get_book_id_not_exist(mock_server, monkeypatch):
    def mock_get_book_by_id(db, id):
        return None

    monkeypatch.setattr(book_service, "get_book_by_id", mock_get_book_by_id)

    response = mock_server.get("/book/99")
    assert response.status_code == 404
    assert response.json()["detail"] == "Book not found"

    response = mock_server.get("/book/0")
    assert response.status_code == 422


def test_update_book(mock_server, monkeypatch):
    book_data = {"title": "something", "description": "something else", "id": 1}
    updated_book_data = {"title": "hello", "description": "something else", "id": 1}

    def mock_get_book_by_id(db, book):
        return book_data

    monkeypatch.setattr(book_service, "get_book_by_id", mock_get_book_by_id)

    def mock_update_book(db, book):
        return updated_book_data

    monkeypatch.setattr(book_service, "update_book", mock_update_book)

    response = mock_server.put("/book/update", data=json.dumps(updated_book_data))
    assert response.status_code == 200
    assert response.json() == updated_book_data


@pytest.mark.parametrize(
    "id, payload, status_code",
    [
        [1, {}, 422],
        [1, {"description": "bar"}, 422],
        [999, {"title": "foo", "description": "bar"}, 404],
        [1, {"title": "1", "description": "bar"}, 422],
        [1, {"title": "foo", "description": "1"}, 422],
    ],
)
def test_update_book_input_validation(mock_server, monkeypatch, id, payload, status_code):
    def mock_get_book_by_id(db, book):
        return None

    monkeypatch.setattr(book_service, "get_book_by_id", mock_get_book_by_id)

    response = mock_server.put("/book/update", data=json.dumps(payload))
    assert response.status_code == status_code


def test_delete_book(mock_server, monkeypatch):
    book_data = {"title": "something", "description": "something else", "id": 1}

    def mock_get_book_by_id(db, book):
        return book_data

    monkeypatch.setattr(book_service, "get_book_by_id", mock_get_book_by_id)

    def mock_remove_book(db, book):
        return book_data

    monkeypatch.setattr(book_service, "remove_book", mock_remove_book)

    response = mock_server.delete("/book/delete/1")
    assert response.status_code == 200
    assert response.json()['message'] == "Deleted successfully"


def test_delete_book_not_exist(mock_server, monkeypatch):
    def mock_get_book_by_id(db, book):
        return None

    monkeypatch.setattr(book_service, "get_book_by_id", mock_get_book_by_id)

    response = mock_server.delete("/book/delete/99")
    assert response.status_code == 404
    assert response.json()["detail"] == "Book not found"

    response = mock_server.delete("/book/delete/0")
    assert response.status_code == 422

