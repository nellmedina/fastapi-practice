from src.fastapi_practice_application import app
from fastapi.testclient import TestClient

import pytest


@pytest.fixture(scope="module")
def mock_server():
    client = TestClient(app)
    yield client
