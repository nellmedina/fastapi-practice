# FastAPI Practice

## How to run

### Docker
```bash
# run a local postgres database
$ docker-compose up
```

### Python
This is using `Pipfile` to install dependencies. If pipenv not installed yet, then do `pip install pipenv` first.

1. pipenv install
2. pipenv shell
3. uvicorn src.fastapi_practice_application:app --reload


## Test using `pytest`

```bash
$ pytest test/
```

## Test using `robotframework`

```bash
# run all tests
$ robot -d logs robot/

# run tests having tag as '@book'
$ robot -d logs -i @get_book .

# run tests having tag as '@functional' and '@nonFunctional' so that it inserts and clears dummy data
$ robot -d logs -i @functional -i @nonFunctional .
```