from sqlalchemy.orm import Session
from src.model.Book import Book
from src.model.Book import BookSchema


def get_book(db: Session, skip: int = 0, limit: int = 100):
    return db.query(Book).offset(skip).limit(limit).all()


def get_book_by_id(db: Session, book_id: int):
    return db.query(Book).filter(Book.id == book_id).first()


def get_book_by_title(db: Session, title: str):
    return db.query(Book).filter(Book.title == title).first()


def create_book(db: Session, book: BookSchema):
    new_book = Book(book.title, book.description)
    db.add(new_book)
    db.commit()
    db.refresh(new_book)
    return new_book


def remove_book(db: Session, book_id: int):
    _book = get_book_by_id(db, book_id)
    db.delete(_book)
    db.commit()


def update_book(db: Session, book: BookSchema):
    new_book = get_book_by_id(db, book.id)

    new_book.title = book.title
    new_book.description = book.description

    db.commit()
    db.refresh(new_book)
    return new_book
