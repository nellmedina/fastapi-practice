from fastapi import APIRouter, Depends, HTTPException, Path
from src.config.app_config import SessionLocal
from sqlalchemy.orm import Session
from src.model.Response import Response
from src.model.Book import BookSchema, BookResponse
from typing import List

from src.service import book_service

router = APIRouter()


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


@router.post("/add", response_model=BookResponse, status_code=201)
async def add_book(book: BookSchema, db: Session = Depends(get_db)):
    new_book = book_service.create_book(db, book)
    return new_book


@router.get("/all", response_model=List[BookResponse])
async def get_books(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    books = book_service.get_book(db, skip, limit)
    return books


@router.get("/{id}/", response_model=BookResponse)
def get_book(db: Session = Depends(get_db), id: int = Path(..., gt=0)):
    book = book_service.get_book_by_id(db, id)
    if not book:
        raise HTTPException(status_code=404, detail="Book not found")
    return book


@router.put("/update", response_model=BookResponse)
async def update_book(book: BookSchema, db: Session = Depends(get_db)):
    old_book = book_service.get_book_by_id(db, book.id)
    if not old_book:
        raise HTTPException(status_code=404, detail="Book not found")

    return book_service.update_book(db, book)


@router.delete("/delete/{id}")
async def delete_book(db: Session = Depends(get_db), id: int = Path(..., gt=0)):
    old_book = book_service.get_book_by_id(db, id)
    if not old_book:
        raise HTTPException(status_code=404, detail="Book not found")

    book_service.remove_book(db, id)
    return Response(status="Ok", code="200", message="Deleted successfully").dict(exclude_none=True)


@router.delete("/delete")
async def delete_book_by_title(book: BookSchema, db: Session = Depends(get_db)):
    old_book = book_service.get_book_by_title(db, book.title)
    if not old_book:
        raise HTTPException(status_code=404, detail="Book not found")

    book_service.remove_book(db, old_book.id)
    return Response(status="Ok", code="200", message="Deleted successfully").dict(exclude_none=True)
