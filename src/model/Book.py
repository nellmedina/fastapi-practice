from sqlalchemy import Column, Integer, String
from src.config.app_config import Base
from pydantic import BaseModel, Field
from typing import Optional


class Book(Base):
    __tablename__ = "book"

    id = Column(Integer, primary_key=True, index=True)
    title = Column(String)
    description = Column(String)

    def __init__(self, title, description):
        self.title = title
        self.description = description


class BookSchema(BaseModel):
    id: Optional[int] = None
    title: str = Field(..., min_length=3, max_length=50)
    description: str = Field(..., min_length=3, max_length=50)


class BookResponse(BookSchema):

    class Config:
        orm_mode = True
