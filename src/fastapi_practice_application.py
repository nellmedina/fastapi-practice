from fastapi import FastAPI
from src.model.Book import Base
from src.controller.book_controller import router
from src.config.app_config import engine

Base.metadata.create_all(bind=engine)

app = FastAPI()

app.include_router(router, prefix="/book", tags=["book"])
