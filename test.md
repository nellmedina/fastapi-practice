## Challenge: Building a simple bank account system

You need to build a simple bank account system that can do the following:
- Create a new bank account with a specified account number, owner name, and initial balance.
- Deposit a specified amount of money into the account.
- Withdraw a specified amount of money from the account.
- Ensure the owner cannot deposit more than $1000 and cannot withdraw more than they have
- Ensure the owner always provides a dollar amount that is at least $0.01
- Print the current balance of the account.

Here are the details of the account system:

**Account**: A class that represents a bank account.
**Fields**:
- **account_number (integer)**: the account number.
- **owner_name (string)**: the owner name of the account.
- **balance (float)**: the current balance of the account.
**Methods**:
- **__init__(self, account_number: int, owner_name: str, initial_balance: float)**: Initializes the account with the specified account number, owner name, and initial balance.
- **deposit(self, amount: float)**: Deposits the specified amount of money into the account.
- **withdraw(self, amount: float)**: Withdraws the specified amount of money from the account.
- **get_balance(self) -> float**: Returns the current balance of the account.
**Unit tests**: Use pytest to write unit tests for the Account class.

You can use the following code as a starting point:

```python
class Account:
    def __init__(self, account_number: int, owner_name: str, initial_balance: float):
        # Initialize fields here
        
    def deposit(self, amount: float):
        # Add amount to the balance
        
    def withdraw(self, amount: float):
        # Subtract amount from the balance
        
    def get_balance(self):
        # Return the current balance
```

The following list of deposits and withdraws should be used to calculate the final balance starting with an initial amount of $1,000:

```python
deposit_amounts = [186.86, 51.73, 737.25, 44.32, 582.76, 128.98, 1648.58, 466.36, 581.71, 233.86]
withdraw_amounts = [19.41, 307.08, 813.91, 1159.19, 831.77, 938.36, 631.32, 1041.1, 73.24, 1337.10]
```

**What is the final account balance?**

## Example Output
Given the following list of values and an initial amount of $1,000:

```python
deposit_amounts = [779.84, 253.39, 1116.28, 789.32, 893.05, 225.81, 718.3, 416.56, 847.0, 209.18]
withdraw_amounts = [501.22, 384.98, 16.0, 1479.16, 673.02, 199.14, 966.3, 518.84, 890.67, 878.0]
```

The output should be as follows:
```
Current balance: $1,000.00

Depositing $779.84 and withdrawing $501.22
Current balance: $1,278.62

Depositing $253.39 and withdrawing $384.98
Current balance: $1,147.03

Depositing $1,116.28 and withdrawing $16.00
Maximum deposit per transaction is 1000.
Skipping deposit for $1,116.28
Current balance: $1,131.03

Depositing $789.32 and withdrawing $1,479.16
Current balance: $441.19

Depositing $893.05 and withdrawing $673.02
Current balance: $661.22

Depositing $225.81 and withdrawing $199.14
Current balance: $687.89

Depositing $718.30 and withdrawing $966.30
Current balance: $439.89

Depositing $416.56 and withdrawing $518.84
Current balance: $337.61

Depositing $847.00 and withdrawing $890.67
Current balance: $293.94

Depositing $209.18 and withdrawing $878.00
Insufficent funds to withdraw $878.00
Skipping withdraw for $878.00
Current balance: $503.12
```
