*** Settings ***
Documentation    This is test suite for testing Book entity
Resource         keywords/get_book_keywords.robot
Force Tags       @get_book

*** Test Cases ***
Test prepare book test data
    [Tags]   @nonFunctional
    ${response}=                    Prepare Book Test Data
    Should Not Be Empty             ${response}

Test get all books
    [Tags]   @functional
    ${books}=                       Get all books
    Should Be Equal As Integers     ${books.status_code}                    200
    Should Not Be Empty             ${books.json()}

Test get book by id
    [Tags]   @functional
    ${books}=                       Get all books
    ${book}=                        Get From List                           ${books.json()}             0
    ${book_id}=                     Get From Dictionary                     ${book}                     id
    ${book_result}=                 Get book by id                          ${book_id}
    Should Be Equal As Integers     ${book_result.status_code}              200
    Should Not Be Empty             ${book_result.json()}

Test get book by id that is invalid
    [Tags]   @functional
    ${book_result}=                 Get book by id with invalid id          8888888
    Should Be Equal As Integers     ${book_result.status_code}              404
    Should Be Equal As Strings      ${book_result.json()['detail']}         ${BOOK_NOT_FOUND_EXCEPTION_MESSAGE}

Test clear book test data
    [Tags]   @nonFunctional
    ${response}=                    Clear Book Test Data
    Should Be Empty                 ${response}