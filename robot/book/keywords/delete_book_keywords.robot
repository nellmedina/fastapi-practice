*** Settings ***
Resource    ../abstract_book_test.robot

*** Keywords ***
Delete a book in database
    [Documentation]                     This is keyword to delete a book in database
    [Arguments]                         ${BOOK_BODY}=${EMPTY}

    Create Session                      fastApi                         ${BOOK_API}
    ${response}=                        DELETE On Session               fastApi             /delete         json=${BOOK_BODY}

    Return From Keyword                 ${EMPTY}