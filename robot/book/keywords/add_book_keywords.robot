*** Settings ***
Resource    ../abstract_book_test.robot

*** Keywords ***
Create a book object
    [Documentation]                 This is keyword to create a book
    [Arguments]                     ${ID}=${EMPTY}
    ...                             ${TITLE}=${EMPTY}
    ...                             ${DESCRIPTION}=${EMPTY}

    &{BOOK}                         Create Dictionary
    ...                             id=${ID}
    ...                             title=${TITLE}
    ...                             description=${DESCRIPTION}

    Return From Keyword             ${BOOK}

Add a book to database
    [Documentation]                 This is keyword to add a book to database
    [Arguments]                     ${BOOK_BODY}=${EMPTY}

    Create Session                  fastApi           ${BOOK_API}
    ${book_response}=                    POST On Session   fastApi          /add    json=${BOOK_BODY}

    Return From Keyword             ${book_response}