*** Settings ***
Resource    ../abstract_book_test.robot
Resource    add_book_keywords.robot
Resource    delete_book_keywords.robot

*** Keywords ***
Get all books
    [Documentation]                 This is keyword to get all books
    Create Session                  fastApi             ${BOOK_API}
    ${book_responses}=                       GET On Session      fastApi     /all
    Return From Keyword             ${book_responses}

Get book by id
    [Documentation]                 This is keyword to get a book by id
    [Arguments]                     ${BOOK_ID}=${EMPTY}
    Create Session                  fastApi                     ${BOOK_API}
    ${book_response}=               GET On Session              fastApi             /${BOOK_ID}
    Return From Keyword             ${book_response}

Get book by id with invalid id
    [Documentation]                 This is keyword to get a book by id
    [Arguments]                     ${BOOK_ID}=${EMPTY}
    Create Session                  fastApi                     ${BOOK_API}
    ${book_response}=               GET On Session              fastApi             /${BOOK_ID}       expected_status=404
    Return From Keyword             ${book_response}

Prepare book test data
    FOR    ${book}    IN    @{books}
       ${id}=                       Get From Dictionary         ${book}             id
       ${title}=                    Get From Dictionary         ${book}             title
       ${description}=              Get From Dictionary         ${book}             description
       ${new_book}                  Create a book object        ${id}               ${title}        ${description}

       Log to console in loop       ${id}                   ${ADDING_LOG_MESSAGE} ${new_book}
       Add a book to database       ${new_book}
    END
    Return From Keyword             @{books}

Clear book test data
    FOR    ${book}    IN    @{books}
       ${id}=                       Get From Dictionary         ${book}             id
       ${title}=                    Get From Dictionary         ${book}             title
       ${description}=              Get From Dictionary         ${book}             description
       ${new_book}                  Create a book object        ${id}               ${title}        ${description}

       Log to console in loop       ${id}                   ${DELETING_LOG_MESSAGE} ${new_book}
       Delete A Book In Database    ${new_book}
    END
    Return From Keyword             ${EMPTY}