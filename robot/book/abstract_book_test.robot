*** Settings ***
Library          RequestsLibrary
Library          Collections
Variables        variables/book_variables.py

*** Variables ***
${BOOK_API}                                 http://localhost:8000/book
${ADDING_LOG_MESSAGE}                       Adding...
${DELETING_LOG_MESSAGE}                     Deleting...
${BOOK_NOT_FOUND_EXCEPTION_MESSAGE}         Book not found
${NEXT_LINE}                                \n


*** Keywords ***
Log to console in loop
    [Documentation]                         This is keyword to log message to console in loop where first index is placed to next line
    [Arguments]                             ${INDEX}=${EMPTY}           ${MESSAGE}=${EMPTY}
    IF    ${INDEX} == 1
        Log To Console                      ${NEXT_LINE}${MESSAGE}
    ELSE
        Log To Console                      ${MESSAGE}
    END